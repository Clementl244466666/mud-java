import java.util.ArrayList;
import java.util.List;

class Box{
    private List<Thing> contents = new ArrayList<Thing>();
    Box(){
        System.out.println("box create");
    }
    public void add (Thing chose){
        this.contents.add(chose);
    } 

    public List<Thing> getContents (){
        return this.contents;
    } 

    public boolean contains (Thing t){
        return  contents.contains(t);
    }

    public void remove(Thing truc) throws RuntimeException{
        boolean ok = this.contents.remove(truc);
        if (!ok) throw new RuntimeException("Impossible d'enlever : "+truc);
    }
       
    
}