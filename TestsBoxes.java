import org.junit.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestsBoxes{
    @Test
    public void testBoxCreate() {
        Box b = new Box();
    }
    @Test
    public void testBoxAdd(){
        Box b = new Box();
        Thing t = new Thing("chose");
        b.add(t);
        assertTrue(b.getContents().contains(t));
    }

    @Test (expected = RuntimeException.class)
    public void testBoxRemoveFail(){
        Box b = new Box();
        Thing t = new Thing("chose");
        b.remove(t);
        assertFalse(b.getContents().contains(t));
    }
}
