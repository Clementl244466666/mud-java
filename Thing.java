


class Thing{
    String name;
    public Thing(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;

    }
    @Override
    public String toString(){
        return "Thing: " + getName();
    } 

    @Override
    public boolean equals(Object obj){
        if (obj instanceof Thing){
            return ((Thing)obj).getName().equals(this.name);
        }
        else return false; 
    }
}